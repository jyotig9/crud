import { Routes, RouterModule } from '@angular/router';

import { CreateStudentComponent } from './create-student/create-student.component';
import { StudentListComponent } from './student-list/student-list.component';

const appRoutes: Routes = [
    { path: '', component: CreateStudentComponent, pathMatch: 'full' },
    { path: 'create-student', component: CreateStudentComponent },
    { path: 'student', component: StudentListComponent }
];

export const routing = RouterModule.forRoot(appRoutes);
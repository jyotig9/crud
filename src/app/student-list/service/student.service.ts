import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Student } from '../../DataModel/student';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class UserService {
    baseApiURL: string = 'http://localhost:4200/api/';

    constructor(private http: HttpClient) { }

    /**
   *  get all Students
   *  */
    public getAll(): Observable<Student> {
        return this.http.get(`${this.baseApiURL}students`);
    }

    public getStudent(studentId) {
        return this.http.get(`${this.baseApiURL + 'students'}/${studentId}`);
    }

    // Add new Student
    public createStudent(student: Student) {
        return this.http.post(`${this.baseApiURL + 'students'}`, student)
    }

    // Delete Student
    public deleteStudent(studentId) {
        return this.http.delete(`${this.baseApiURL + 'students'}/${studentId}`)
    }

    public updateStudent(student: Student) {
        return this.http.put(`${this.baseApiURL + 'students'}/${student.id}`, student)
    }

    public editStudent(studentId) {
        return this.http.get(`${this.baseApiURL + 'students'}/${studentId}`)
    }

}
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({providedIn: 'root'})
export class SharedService {

  private messageSource = new BehaviorSubject({});
  currentMessage = this.messageSource.asObservable();

  constructor() { }

// Using This function I am sharing Data Between Student List Component and Create Student component 
  changeMessage(message:any) {
    this.messageSource.next(message)
  }

}
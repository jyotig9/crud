import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { Student } from '../DataModel/student';
import { UserService } from '../student-list/service/student.service'
import { Router } from '@angular/router';
import { SharedService } from "src/app/student-list/service/shared.service";


/**
 * @title Table with sorting
 */
@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css'],
})
export class StudentListComponent implements OnInit {
  public editObject: Student;

  constructor(private router: Router, private userService: UserService, private sharedService: SharedService) {
  }

  displayedColumns: string[] = ['firstName', 'lastName', 'address', 'email', 'password', 'dateOfBirth', 'action'];

  dataSource = new MatTableDataSource();

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.getAllStudentList();
  }

  getAllStudentList() {
    this.userService.getAll().subscribe((data: Student[]) => {
      this.dataSource = new MatTableDataSource(data);
      console.log("get all data", this.dataSource)
    }, errr => {
      console.log(errr)
    })
  }

  editStudent(studentId) {
    this.userService.editStudent(studentId).subscribe((ret) => {
      this.editObject = ret;
      this.sharedService.changeMessage(this.editObject)
      this.router.navigate(['/create-student']);
    })
  }

  deleteStudent(studentId) {
    debugger
    this.userService.deleteStudent(studentId).subscribe((ret) => {
      console.log("Policy deleted: ", ret);
    })
  }
}

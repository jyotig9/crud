import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { InMemoryWebApiModule } from "angular-in-memory-web-api";  
import { DataService } from '../app/data.service';
// import {MatFormFieldModule} from '@angular/material/form-field';
// import {MatInputModule} from '@angular/material/input';
import { MatTableModule} from '@angular/material/table'
import {MatDatepickerModule,MatNativeDateModule, MatFormFieldModule, MatInputModule} from '@angular/material';
import { AppComponent } from './app.component';
import { CreateStudentComponent } from './create-student/create-student.component';
import { StudentListComponent } from './student-list/student-list.component';
import { routing } from './app.route'

@NgModule({
  declarations: [
    AppComponent,
    CreateStudentComponent,
    StudentListComponent
  ],
  imports: [
    BrowserModule,HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,MatTableModule,
    MatFormFieldModule,MatInputModule,MatDatepickerModule,MatNativeDateModule, MatFormFieldModule, MatInputModule,
    routing,
    InMemoryWebApiModule.forRoot(DataService)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

export class Student {
    id: number;
    firstName: string;
    lastName: string;
    address: string;
    email:string;
    password:string;
    dateOfBirth:string
}
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { UserService } from '../student-list/service/student.service';
import { SharedService } from "src/app/student-list/service/shared.service";
import { Router } from '@angular/router';
// import custom validator to validate that password and confirm password fields match
//import { MustMatch } from './_helpers/must-match.validator';
import { Student } from '../DataModel/student'
@Component({
  selector: 'app-create-student',
  templateUrl: './create-student.component.html',
  styleUrls: ['./create-student.component.css'],
  providers: [UserService]
})
export class CreateStudentComponent implements OnInit {
  registerForm: FormGroup;
  submitted: boolean;
  studentList: Student[];
  isEdit: boolean;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private sharedService: SharedService, private router: Router) { }

  ngOnInit() {

    this.createForm();

    // This methode used to fill editable record into Html Form 
    this.sharedService.currentMessage.subscribe((student: Student) => {
      if (this.isEmpty(student)) {
        // isEdit used for Hide/Show "Add New Student"
        this.isEdit = false;
      } else {
        this.isEdit = true;
         // set student data into form
        this.registerForm.setValue({
          id: student.id,
          firstName: student.firstName,
          lastName: student.lastName,
          address: student.address,
          email: student.email,
          password: student.password,
          dateOfBirth: student.dateOfBirth
        })
      }
    })
  }

  //Check object empty
  isEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key))
        return false;
    }
    return true;
  }

  // Create Forms Fields
  createForm() {
    this.registerForm = this.formBuilder.group({
      id: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      dateOfBirth: ['', [Validators.required]]
    });

  }

  // This methode is used to create new Record
  public createStudent() {
    let len = 1;
    let student: Student = {
      "id": len + 22,
      "firstName": this.registerForm.controls.firstName.value,
      "lastName": this.registerForm.controls.lastName.value,
      "address": this.registerForm.controls.address.value,
      "email": this.registerForm.controls.email.value,
      "password": this.registerForm.controls.password.value,
      "dateOfBirth": this.registerForm.controls.dateOfBirth.value
    }
    this.userService.createStudent(student).subscribe((ret) => {
      alert("Student created");
    });
  }


  // This method used for update record
  public updateStudent() {
    let student: Student = {
      "id": this.registerForm.controls.id.value,
      "firstName": this.registerForm.controls.firstName.value,
      "lastName": this.registerForm.controls.lastName.value,
      "address": this.registerForm.controls.address.value,
      "email": this.registerForm.controls.email.value,
      "password": this.registerForm.controls.password.value,
      "dateOfBirth": this.registerForm.controls.dateOfBirth.value
    }
    this.userService.updateStudent(student).subscribe((ret) => {
      alert("Student Updated")
    });
  }

  // Share Student data 
  viewStudentList() {
    this.sharedService.changeMessage({});
    this.router.navigate(['/student']);
  }
}

import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
@Injectable({
  providedIn: 'root'
})
export class DataService implements InMemoryDbService {

  constructor() { }
  createDb() {

    let students = [
      { id: 1, firstName: 'jyoti', lastName: 'ghadage', address: 'marke jjerfeiuwrw', email: 'gha@gmail.com', password: '123456789', dateOfBirth: '12/02/2091' },
      { id: 2, firstName: 'nanda', lastName: "shing", address: 'ehfuseh jsefiuhisuehfu', email: 'ghatt6787@gmail.com', password: '123456789', dateOfBirth: '' },
      { id: 3, firstName: 'PO1', lastName: 1000, address: 'jfhdr hhfiuefu buefhiueshf', email: 'Insurance policy number PO1', password: '', dateOfBirth: '' },
      { id: 4, firstName: 'PO1', lastName: 1000, address: 'fjiuefdsfd', email: 'Insurance policy number PO1', password: '', dateOfBirth: '' },
    ];

    return { students };

  }
}
